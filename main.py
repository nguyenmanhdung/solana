
from flask import Flask, request, jsonify


app = Flask(__name__)


@app.route('/', methods=['GET'])
def perform_inference():
    data =  {"actinic keratoses": 8.436448e-06,
             "basal cell carcinoma": 0.00025271263,
             "benign keratosis-like lesions": 3.6649337e-05,
             "dermatofibroma": 4.34296e-06,
             "melanoma": 0.02392229,
             "melanocytic": 0.97577536,
             "vascular lesions": 2.015278e-07}
    return jsonify({"data": data})


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=80)
